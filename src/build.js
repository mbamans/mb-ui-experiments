const { join } = require('path');
const { writeFileSync, mkdirSync, rmdirSync } = require('fs');
try {
    rmdirSync(
        join(__dirname, '..', 'dist'),
        {
            recursive: true
        }
    );
} catch (error) { 
    console.log(error);
}

mkdirSync(
    join(__dirname, '..', 'dist')
);
writeFileSync(
    join(__dirname, '..', 'dist', `app.${Date.now()}.bundle.js`),
    `
        console.log("Hello World");
    `,
    {
        encoding: 'utf-8'
    }
);
